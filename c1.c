#include <stdio.h>
#include <stdlib.h>

#define fixedperfomancecost 500
#define attendeecost 3

int Noofattendees(double);
double Income(double);
double expenditure(double);
double profit(double);

int Noofattendees(double price)
{
    return 120-(((price-15)/5)*20);
}
double Income(double price)
{
    return Noofattendees(price)*price ;
}
double expenditure(double price)
{
    return fixedperfomancecost+(Noofattendees(price)*attendeecost);
}
double profit(double price)
{
    return Income(price)- expenditure(price);
}

int main()
{
    double price;
    printf("\Profit expected from Ticket Prices :\n\n");
    for(price=5;price<50;price+=5)
    {
        printf("The Ticket Price = Rs.%.2f\t   The Profit = Rs.%.2f\n",price,profit(price));
        printf("----------------------------------------------------------------\n");
    }
    return 0;
}
